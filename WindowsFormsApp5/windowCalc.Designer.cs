﻿
namespace WindowsFormsApp5
{
    partial class windowCalc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sumButton = new System.Windows.Forms.Button();
            this.sayi1Input = new System.Windows.Forms.TextBox();
            this.subtractButton = new System.Windows.Forms.Button();
            this.multipleButton = new System.Windows.Forms.Button();
            this.divideButton = new System.Windows.Forms.Button();
            this.sayi2Input = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.sonucLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // sumButton
            // 
            this.sumButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.sumButton.Location = new System.Drawing.Point(264, 42);
            this.sumButton.Name = "sumButton";
            this.sumButton.Size = new System.Drawing.Size(25, 23);
            this.sumButton.TabIndex = 0;
            this.sumButton.Text = "+";
            this.sumButton.UseVisualStyleBackColor = true;
            this.sumButton.Click += new System.EventHandler(this.sumButton_Click);
            this.sumButton.MouseEnter += new System.EventHandler(this.sumButton_MouseEnter);
            this.sumButton.MouseLeave += new System.EventHandler(this.sumButton_MouseLeave);
            // 
            // sayi1Input
            // 
            this.sayi1Input.Location = new System.Drawing.Point(124, 42);
            this.sayi1Input.Name = "sayi1Input";
            this.sayi1Input.Size = new System.Drawing.Size(100, 20);
            this.sayi1Input.TabIndex = 1;
            // 
            // subtractButton
            // 
            this.subtractButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.subtractButton.Location = new System.Drawing.Point(264, 71);
            this.subtractButton.Name = "subtractButton";
            this.subtractButton.Size = new System.Drawing.Size(25, 23);
            this.subtractButton.TabIndex = 2;
            this.subtractButton.Text = "---";
            this.subtractButton.UseVisualStyleBackColor = true;
            this.subtractButton.Click += new System.EventHandler(this.subtractButton_Click);
            this.subtractButton.MouseEnter += new System.EventHandler(this.subtractButton_MouseEnter);
            this.subtractButton.MouseLeave += new System.EventHandler(this.subtractButton_MouseLeave);
            // 
            // multipleButton
            // 
            this.multipleButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.multipleButton.Location = new System.Drawing.Point(264, 100);
            this.multipleButton.Name = "multipleButton";
            this.multipleButton.Size = new System.Drawing.Size(25, 23);
            this.multipleButton.TabIndex = 3;
            this.multipleButton.Text = "x";
            this.multipleButton.UseVisualStyleBackColor = true;
            this.multipleButton.Click += new System.EventHandler(this.multipleButton_Click);
            this.multipleButton.MouseEnter += new System.EventHandler(this.multipleButton_MouseEnter);
            this.multipleButton.MouseLeave += new System.EventHandler(this.multipleButton_MouseLeave);
            // 
            // divideButton
            // 
            this.divideButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.divideButton.Location = new System.Drawing.Point(264, 129);
            this.divideButton.Name = "divideButton";
            this.divideButton.Size = new System.Drawing.Size(25, 23);
            this.divideButton.TabIndex = 4;
            this.divideButton.Text = "/";
            this.divideButton.UseVisualStyleBackColor = true;
            this.divideButton.Click += new System.EventHandler(this.divideButton_Click);
            this.divideButton.MouseEnter += new System.EventHandler(this.divideButton_MouseEnter);
            this.divideButton.MouseLeave += new System.EventHandler(this.divideButton_MouseLeave);
            // 
            // sayi2Input
            // 
            this.sayi2Input.Location = new System.Drawing.Point(124, 71);
            this.sayi2Input.Name = "sayi2Input";
            this.sayi2Input.Size = new System.Drawing.Size(100, 20);
            this.sayi2Input.TabIndex = 5;
            this.sayi2Input.TextChanged += new System.EventHandler(this.sayi2Input_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(65, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Sayı 1 :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.Location = new System.Drawing.Point(65, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Sayı 2 :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.Location = new System.Drawing.Point(65, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Sonuç";
            // 
            // sonucLabel
            // 
            this.sonucLabel.AutoSize = true;
            this.sonucLabel.Location = new System.Drawing.Point(121, 129);
            this.sonucLabel.Name = "sonucLabel";
            this.sonucLabel.Size = new System.Drawing.Size(19, 13);
            this.sonucLabel.TabIndex = 9;
            this.sonucLabel.Text = "....";
            // 
            // windowCalc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SeaShell;
            this.ClientSize = new System.Drawing.Size(449, 425);
            this.Controls.Add(this.sonucLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.sayi2Input);
            this.Controls.Add(this.divideButton);
            this.Controls.Add(this.multipleButton);
            this.Controls.Add(this.subtractButton);
            this.Controls.Add(this.sayi1Input);
            this.Controls.Add(this.sumButton);
            this.Name = "windowCalc";
            this.Text = "windowCalc";
            this.Load += new System.EventHandler(this.windowCalc_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button sumButton;
        private System.Windows.Forms.TextBox sayi1Input;
        private System.Windows.Forms.Button subtractButton;
        private System.Windows.Forms.Button multipleButton;
        private System.Windows.Forms.Button divideButton;
        private System.Windows.Forms.TextBox sayi2Input;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label sonucLabel;
    }
}