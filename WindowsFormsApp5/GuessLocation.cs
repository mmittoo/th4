﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace guesser
{
    public class GuessLocation
    {
        public int minX { get; set; } = 0;
        public int minY { get; set; } = 0;
        public int maxX { get; set; } = 720;
        public int maxY { get; set; } = 720;

        public GuessLocation()
        {

        }

    }
}
