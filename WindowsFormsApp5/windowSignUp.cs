﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp5
{ 
    public partial class windowSignUp : Form
    {
        Thread th;
        private string currentPassword;
        private string usernamee;
        private void windowSignUp_Load(object sender, EventArgs e)
        {

        }
        public windowSignUp()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string pass1 = password.Text;
            string pass2 = passwordConfirm.Text;
            if(username.Text !="" && password.Text != "" && passwordConfirm.Text != "")
            {
            if (passIsSame(pass1, pass2))
                {
                    usernamee = username.Text;
                    currentPassword = md5(pass1);
                    alertLabel.Text = "you signed up successfully!";
                    alertLabel.BackColor = Color.LightGreen;
                   /* windowLogin formLogIn = new windowLogin("12", "32");
                   // formLogIn.ShowDialog();*/
                    timer1.Start();
                }
                else
                {
                    alertLabel.Text = "the passwords you entered do not match";
                    alertLabel.BackColor = Color.IndianRed;
                }
            }
            else
            {
                alertLabel.Text = "please fill in the required fields";
                alertLabel.BackColor = Color.IndianRed;
            }
            

        }


        private static string md5(string password)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            byte[] bytes = md5.ComputeHash(Encoding.UTF8.GetBytes(password.ToCharArray()));

            StringBuilder builder = new StringBuilder();

            foreach (var item in bytes)
            {
                builder.Append(item.ToString("x3"));
            }

            return builder.ToString();
        }

        private bool passIsSame(string password, string confirmedPassword)
        {
            bool passIsSame = false;
            if(md5(password) == md5(confirmedPassword))
            {
                passIsSame = true;
            }
            return passIsSame;
        }

        private void password_TextChanged(object sender, EventArgs e)
        {
            password.PasswordChar = '*';
        }

        private void passwordConfirm_TextChanged(object sender, EventArgs e)
        {
            passwordConfirm.PasswordChar = '*';
        }

        
      
        private void openNewForm(object obj)
        {
            Application.Run(new windowLogin(usernamee, currentPassword));       
        }
        private  int ticks = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            
            ticks++;
            if (ticks == 3)
            {
                this.Text = ticks.ToString();
                this.Close();
                th = new Thread(openNewForm);
                th.SetApartmentState(ApartmentState.STA);
                th.Start();

            }
        }
    }
}
