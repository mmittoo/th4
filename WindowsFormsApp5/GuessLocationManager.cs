﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace guesser
{
    public class GuessLocationManager : IGuessLocation
    {
        public int[] generateArandomLocation(int minX, int minY, int maxX, int maxY)
        {
            Random rnd = new Random();
            int onXdirection = rnd.Next(minX, maxX);
            int onYdirection = rnd.Next(minY, maxY);
            int[] position = { onXdirection, onYdirection };
            return position;
        }

        public void resetLimits(GuessLocation guessLocation)
        {
            
            guessLocation.minY = 0;
            guessLocation.maxY = 720;
            guessLocation.minX = 0;
            guessLocation.maxX = 720;

        }
    }
}
