﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using System.Security.Cryptography;
using System.Text;

namespace WindowsFormsApp5
{ 
    
    public partial class windowLogin : Form
    {
        string encPassword;
        string usernamee;      
        Thread th;
        
       public windowLogin(string username, string password)
        {
            InitializeComponent();
            encPassword = password;
            usernamee = username;
        }
     

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            bool loggedIn = false;
            
            if(usernameTextBox.Text == usernamee && md5(passwordTextBox.Text) == encPassword)
                {
                      uyariMesaji.Text = "You are successfully logged in";
                      uyariMesaji.BackColor = Color.LightGreen;
                      loggedIn = true;
                                       
                }  
            else
                {
                      uyariMesaji.Text = "Wrong username or password";
                      uyariMesaji.BackColor = Color.IndianRed;
                }

            
           
            if (loggedIn)
            {
                
                timer2.Start();           //if logged in, timer starts counting
            }

            

        }


        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void passwordTextBox_TextChanged(object sender, EventArgs e)
        {
            passwordTextBox.PasswordChar = '*';          //password should not to be visible
        }
        private int ticks;
        private void timer2_Tick(object sender, EventArgs e)
        {
            ticks++;
            if (ticks == 3)
            {
                this.Close();
                th = new Thread(openNewForm);
                th.SetApartmentState(ApartmentState.STA);
                th.Start();

            }

        }

        private static string md5(string password)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            byte[] bytes = md5.ComputeHash(Encoding.UTF8.GetBytes(password.ToCharArray()));

            StringBuilder builder = new StringBuilder();

            foreach (var item in bytes)
            {
                builder.Append(item.ToString("x3"));
            }

            return builder.ToString();
        }



        private void openNewForm(object obj)
        {
            Application.Run(new Applications());        //it will come from other team made
        }
    }
}
