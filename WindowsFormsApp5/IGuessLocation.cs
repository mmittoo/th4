﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace guesser
{
    public interface IGuessLocation
    {
        int[] generateArandomLocation(int minX, int minY, int maxX,int maxY);

       void resetLimits(GuessLocation guessLocation);
    }
}
