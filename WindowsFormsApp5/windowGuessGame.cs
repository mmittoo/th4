﻿using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lannyeteramk
{
    public partial class Form1 : Form
    {
        GuessLocation guessLocation = new GuessLocation();
        GuessLocationManager guessLocationManager = new GuessLocationManager();
        
     
        public Form1()
        {
            
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
           locationLabel.Text = guessCursor.Location.X.ToString()+ ","+
                                 guessCursor.Location.Y.ToString();

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

       
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_Load_1(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            
        }


        private void upButton_Click_1(object sender, EventArgs e)
        {
            
            guessLocation.maxY = guessCursor.Location.Y;
            int x = guessLocationManager.generateArandomLocation(guessLocation.minX, guessLocation.minY,
                                                        guessLocation.maxX, guessLocation.maxY)[0];
         
            int y = guessLocationManager.generateArandomLocation(guessLocation.minX, guessLocation.minY,
                                                        guessLocation.maxX, guessLocation.maxY)[1];
            guessCursor.Location = new Point(x, y);
        }

        private void leftButton_Click(object sender, EventArgs e)
        {
            guessLocation.maxX = guessCursor.Location.X;
            int x = guessLocationManager.generateArandomLocation(guessLocation.minX, guessLocation.minY,
                                                        guessLocation.maxX, guessLocation.maxY)[0];
           
            int y = guessLocationManager.generateArandomLocation(guessLocation.minX, guessLocation.minY,
                                                        guessLocation.maxX, guessLocation.maxY)[1];
            guessCursor.Location = new Point(x, y);

            
        }

        private void rightButton_Click(object sender, EventArgs e)
        {
            guessLocation.minX= guessCursor.Location.X;
            int x = guessLocationManager.generateArandomLocation(guessLocation.minX, guessLocation.minY,
                                                        guessLocation.maxX, guessLocation.maxY)[0];
            
            int y = guessLocationManager.generateArandomLocation(guessLocation.minX, guessLocation.minY,
                                                        guessLocation.maxX, guessLocation.maxY)[1];
            guessCursor.Location = new Point(x, y);
            
        }

        private void downButton_Click(object sender, EventArgs e)
        {
            guessLocation.minY = guessCursor.Location.Y;
            int x = guessLocationManager.generateArandomLocation(guessLocation.minX, guessLocation.minY,
                                                        guessLocation.maxX, guessLocation.maxY)[0];
           
            int y = guessLocationManager.generateArandomLocation(guessLocation.minX, guessLocation.minY,
                                                        guessLocation.maxX, guessLocation.maxY)[1];
            guessCursor.Location = new Point(x, y);
            
        }

        private void resetButton_Click(object sender, EventArgs e)
        {
            //guessLocation.minY = 0;
            //guessLocation.minX = 0;
            //guessLocation.maxX = 720;                               //I'm lost between reference and data types. I would work on if I had time
            //guessLocation.maxY = 720;
            reset();
            guessCursor.Location = new Point(45, 45);
        }

        public void reset()
        {
            guessLocation.minY = 0;
            guessLocation.minX = 0;
            guessLocation.maxX = 720;
            guessLocation.maxY = 720;

        }


        
    }
}
