﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WindowsFormsApp5
{
    class Calculator
    {
        public double number1, number2;

        public Calculator(double number1, double number2)
        {
            this.number1 = number1;
            this.number2 = number2;
        }

        public double sumElements(double num1, double num2)
        {
            return num1 + num2;
        }
        public double extractElements(double num1, double num2)
        {
            return num1 - num2;
        }
        public double multipleElements(double num1, double num2)
        {
            return num1 * num2;
        }
        public double divideElements(double num1, double num2)
        {
            if(num2 == 0)
            {
                return -1;           //will be controlled in the otherside
            }
            else
            {
                return num1 / num2;
            }
            
        }

    }
}
